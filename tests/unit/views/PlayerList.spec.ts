import { shallowMount } from '@vue/test-utils'

import PlayerList from '@/views/PlayerList.vue'

jest.mock('vue-router', () => ({
  useRoute: jest.fn(() => ({
    name: 'PlayerList',
    query: {
      page: '1',
      per_page: '5',
    },
  })),
  useRouter: jest.fn(() => ({ push: jest.fn() })),
}))

describe('PlayerList.vue', () => {
  it('matches snapshot', () => {
    const wrapper = shallowMount(PlayerList)

    expect(wrapper).toMatchSnapshot()
  })
})
