import { shallowMount } from '@vue/test-utils'
import PlayerView from '@/views/PlayerView.vue'

jest.mock('vue-router', () => ({
  useRoute: jest.fn(() => ({
    name: 'PlayerView',
    params: {
      id: '1',
    },
  })),
}))

jest.mock('@/composables/chart', () => ({
  useLineChart: jest.fn(() => ({
    render: jest.fn(),
  })),
}))

describe('PlayerView.vue', () => {
  it('matches snapshot', () => {
    const wrapper = shallowMount(PlayerView)
    expect(wrapper).toMatchSnapshot()
  })
})
