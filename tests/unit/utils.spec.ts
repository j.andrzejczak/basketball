import { footInches, arrayFromRange } from '@/utils'

describe('footInches', () => {
  it('composes foots and inches', () => {
    const value = footInches(6, 2)

    expect(value).toBe('6\' 2"')
  })
})

describe('arrayFromRange', () => {
  it('create array with default params', () => {
    const value = arrayFromRange()

    expect(value).toEqual(expect.arrayContaining([0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]))
  })

  it('create array with params', () => {
    const value = arrayFromRange(1, 3)

    expect(value).toEqual(expect.arrayContaining([1, 2, 3]))
  })
})
