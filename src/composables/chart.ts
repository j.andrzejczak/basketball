import { watch, Ref } from 'vue'
import ApexCharts from 'apexcharts'

export function useLineChart(element: HTMLElement, label: string, seriesRef: Ref<number[]>, categories: number[]): { render: () => void } {
  const options = {
    chart: {
      type: 'line',
    },
    series: [
      {
        name: label,
        data: [],
      },
    ],
    xaxis: {
      categories: categories,
    },
  }

  const c = new ApexCharts(element, options)

  watch(seriesRef, (series) => {
    if (series.length === categories.length) {
      c.updateSeries([
        {
          data: series,
        },
      ])
    }
  })

  function render() {
    c.render()
  }

  return { render }
}
