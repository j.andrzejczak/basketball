import { createStore, Store as VuexStore, CommitOptions, DispatchOptions, MutationTree, ActionContext, ActionTree, GetterTree } from 'vuex'
import { getPlayerList, getPlayer, getSeasonAverages, Player, PaginationMeta, PlayersParams, SeasonAverages } from '@/service/basketballApi'
import { arrayFromRange } from '@/utils'

export type SeasonAveragesStore = {
  [season: number]: SeasonAverages
}

export enum LoadingState {
  INITIAL = 'INITIAL',
  LOADING = 'LOADING',
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
}

export type State = {
  players: Player[]
  playersLoading: LoadingState
  playersPagination: PaginationMeta
  singlePlayer: Player | null
  singlePlayerLoading: boolean
  seasonAverages: SeasonAveragesStore
  seasonAveragesLoading: LoadingState
}

const state: State = {
  players: [],
  playersLoading: LoadingState.INITIAL,
  playersPagination: {
    total_pages: 0,
    current_page: 0,
    next_page: 0,
    per_page: 0,
    total_count: 0,
  },
  singlePlayer: null,
  singlePlayerLoading: false,
  seasonAverages: {},
  seasonAveragesLoading: LoadingState.INITIAL,
}

export enum MutationType {
  SetPlayers = 'SET_PLAYERS',
  SetPlayersLoading = 'SET_PLAYERS_LOADING',
  SetPlayersPagination = 'SET_PLAYERS_PAGINATION',
  SetSinglePlayer = 'SET_SINGLE_PLAYER',
  SetSinglePlayerLoading = 'SET_SINGLE_PLAYER_LOADING',
  AddSeasonAverages = 'ADD_SEASON_AVERAGES',
  RemoveSeasonAverages = 'REMOVE_SEASON_AVERAGES',
  SetSeasonAveragesLoading = 'SET_SEASON_AVERAGES_LOADING',
}

export type Mutations = {
  [MutationType.SetPlayers](state: State, value: Player[]): void
  [MutationType.SetPlayersLoading](state: State, value: LoadingState): void
  [MutationType.SetPlayersPagination](state: State, value: PaginationMeta): void
  [MutationType.SetSinglePlayer](state: State, value: Player | null): void
  [MutationType.SetSinglePlayerLoading](state: State, value: boolean): void
  [MutationType.AddSeasonAverages](state: State, value: SeasonAverages): void
  [MutationType.RemoveSeasonAverages](state: State): void
  [MutationType.SetSeasonAveragesLoading](state: State, value: LoadingState): void
}

const mutations: MutationTree<State> & Mutations = {
  [MutationType.SetPlayers](state, value) {
    state.players = value
  },
  [MutationType.SetPlayersLoading](state, value) {
    state.playersLoading = value
  },
  [MutationType.SetPlayersPagination](state, value) {
    state.playersPagination = value
  },
  [MutationType.SetSinglePlayer](state, value) {
    state.singlePlayer = value
  },
  [MutationType.SetSinglePlayerLoading](state, value) {
    state.singlePlayerLoading = value
  },
  [MutationType.AddSeasonAverages](state, value) {
    const { season } = value

    if (season) {
      state.seasonAverages[season] = value
    }
  },
  [MutationType.RemoveSeasonAverages](state) {
    state.seasonAverages = {}
  },
  [MutationType.SetSeasonAveragesLoading](state, value) {
    state.seasonAveragesLoading = value
  },
}

export enum ActionTypes {
  GetPlayerList = 'GET_PLAYER_LIST',
  GetPlayer = 'GET_PLAYER',
  GetSeasonAverages = 'GET_SEASON_AVERAGES',
}

type ActionArguments = Omit<ActionContext<State, State>, 'commit'> & {
  commit<K extends keyof Mutations>(key: K, payload?: Parameters<Mutations[K]>[1]): ReturnType<Mutations[K]>
}

export type Actions = {
  [ActionTypes.GetPlayerList](context: ActionArguments, params: PlayersParams): void
  [ActionTypes.GetPlayer](context: ActionArguments, params: { id: number }): void
  [ActionTypes.GetSeasonAverages](context: ActionArguments, params: { id: number; from: number; to: number }): void
}

const actions: ActionTree<State, State> & Actions = {
  async [ActionTypes.GetPlayerList]({ commit }, params) {
    commit(MutationType.SetPlayersLoading, LoadingState.INITIAL)
    commit(MutationType.SetPlayersLoading, LoadingState.LOADING)

    try {
      const response = await getPlayerList(params)
      const { data, meta } = response.data

      commit(MutationType.SetPlayersLoading, LoadingState.SUCCESS)
      commit(MutationType.SetPlayers, data)
      commit(MutationType.SetPlayersPagination, meta)
    } catch (e) {
      commit(MutationType.SetPlayersLoading, LoadingState.ERROR)
    }
  },
  async [ActionTypes.GetPlayer]({ commit }, params) {
    commit(MutationType.SetSinglePlayerLoading, true)

    const response = await getPlayer(params.id)
    const { data } = response

    commit(MutationType.SetSinglePlayerLoading, false)
    commit(MutationType.SetSinglePlayer, data)
  },
  async [ActionTypes.GetSeasonAverages]({ commit }, { id, from, to }) {
    commit(MutationType.SetSeasonAveragesLoading, LoadingState.INITIAL)
    commit(MutationType.SetSeasonAveragesLoading, LoadingState.LOADING)
    commit(MutationType.RemoveSeasonAverages)

    arrayFromRange(from, to).forEach(async (season) => {
      const response = await getSeasonAverages({ player_ids: [id.toString()], season: season.toString() })
      const { data } = response.data

      if (data.length) {
        const [seasonAverages] = data

        commit(MutationType.AddSeasonAverages, seasonAverages)
      }
    })

    commit(MutationType.SetSeasonAveragesLoading, LoadingState.SUCCESS)
  },
}

export type Getters = {
  playerListWithoutTeamName(state: State): Omit<Player, 'team'>[]
  playerGamesPerSeason(state: State): { season: number; games: number }[]
}

const getters: GetterTree<State, State> & Getters = {
  playerListWithoutTeamName(state) {
    return state.players.map(({ team, ...player }) => player)
  },
  playerGamesPerSeason(state) {
    return Object.entries(state.seasonAverages)
      .map(([s, { games_played }]) => ({ season: +s, games: games_played }))
      .sort((a, b) => a.season - b.season)
  },
}

export const store = createStore<State>({ state, mutations, actions, getters })

export type Store = Omit<VuexStore<State>, 'getters' | 'commit' | 'dispatch'> & {
  commit<K extends keyof Mutations, P extends Parameters<Mutations[K]>[1]>(
    key: K,
    payload: P,
    options?: CommitOptions
  ): ReturnType<Mutations[K]>
} & {
  dispatch<K extends keyof Actions>(key: K, payload?: Parameters<Actions[K]>[1], options?: DispatchOptions): ReturnType<Actions[K]>
} & { getters: { [K in keyof Getters]: ReturnType<Getters[K]> } }

export const useStore = (): Store => store as Store
