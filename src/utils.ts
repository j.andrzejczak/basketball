import { LocationQueryValue } from 'vue-router'

export const footInches = (foot: string | number | null = null, inch: string | number | null = null): string => {
  return [foot ? `${foot}'` : '', inch ? `${inch}"` : ''].join(' ').trim()
}

export const convertLocationQueryValue = (value: LocationQueryValue | LocationQueryValue[]): string | undefined =>
  value ? (Array.isArray(value) ? [value] : value).toString() : undefined

export const arrayFromRange = (from = 0, to = 10): number[] =>
  Array(to - from + 1)
    .fill(from)
    .map((n, i) => n + i)
