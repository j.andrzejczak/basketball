import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import PlayerList from '../views/PlayerList.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/players',
    name: 'PlayerList',
    component: PlayerList,
  },
  {
    path: '/player/:id',
    name: 'PlayerView',
    component: () => import(/* webpackChunkName: "about" */ '../views/PlayerView.vue'),
  },
  { path: '/', redirect: '/players' },
  { path: '/player', redirect: '/players' },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

export default router
