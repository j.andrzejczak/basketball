import axios, { AxiosResponse } from 'axios'

const baseURL = 'https://www.balldontlie.io/api/v1/'

const basketballApi = axios.create({ baseURL })

export type PaginationMeta = {
  total_pages: number
  current_page: number
  next_page: number
  per_page: number
  total_count: number
}

export type Team = {
  id: number
  abbreviation: string
  city: string
  conference: string
  division: string
  full_name: string
  name: string
}

export type Player = {
  id: number
  first_name: string
  last_name: string
  position: string
  height_feet?: number
  height_inches?: number
  weight_pounds?: number
  team: Team
}

export type Players = {
  data: Player[]
  meta: PaginationMeta
}

export type PlayersParams = {
  page?: string | null
  per_page?: string | null
}

export type SeasonAverages = {
  games_played: number
  player_id: number
  season: number
  min: string
  fgm: number
  fga: number
  fg3m: number
  fg3a: number
  ftm: number
  fta: number
  oreb: number
  dreb: number
  reb: number
  ast: number
  stl: number
  blk: number
  turnover: number
  pf: number
  pts: number
  fg_pct: number
  fg3_pct: number
  ft_pct: number
}

export type SeasonAveragesResponse = {
  data: SeasonAverages[]
}

export type SeasonAveragesParams = {
  player_ids?: string[]
  season?: string | null
}

export const getPlayerList = (params: PlayersParams): Promise<AxiosResponse<Players>> => basketballApi.get<Players>('players', { params })
export const getPlayer = (id: number): Promise<AxiosResponse<Player>> => basketballApi.get<Player>(`players/${id}`)
export const getSeasonAverages = (params: SeasonAveragesParams): Promise<AxiosResponse<SeasonAveragesResponse>> =>
  basketballApi.get<SeasonAveragesResponse>('season_averages', { params })
